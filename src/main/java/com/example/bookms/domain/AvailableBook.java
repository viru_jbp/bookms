package com.example.bookms.domain;

public class AvailableBook {

    private Integer bookId;
    private String isbn;
    private String author;
    private Integer availableCopies;

    public AvailableBook(Integer bookId, String isbn, String author, Integer availableCopies) {
        this.bookId = bookId;
        this.isbn = isbn;
        this.author = author;
        this.availableCopies = availableCopies;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getAvailableCopies() {
        return availableCopies;
    }

    public void setAvailableCopies(Integer availableCopies) {
        this.availableCopies = availableCopies;
    }
}
