package com.example.bookms.endpoint;

import com.example.bookms.domain.AvailableBook;
import com.example.bookms.domain.Book;
import com.example.bookms.repo.BookRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class BookResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookResource.class);

    @Autowired
    BookRepo bookRepo;

    @GetMapping("/allBooks")
    public List<Book> getAllBooks(){
        return bookRepo.findAll();
    }

    @GetMapping("/allAvailableBooks")
    public List<AvailableBook> getAllAvailableBooks(){
        LOGGER.info("Fetching all student from DB");
        return bookRepo.findAll().stream().map(book -> new AvailableBook(book.getId(),
                        book.getIsbn(), book.getAuthor(), book.getTotalCopies() - book.getIssuedCopies())).
                collect(Collectors.toList());

//        List<AvailableBook> availableBookList = bookList.stream().map(book -> new AvailableBook(book.getId(),
//                book.getIsbn(), book.getAuthor(), book.getTotalCopies() - book.getIssuedCopies())).
//                collect(Collectors.toList());

  /*      List<UserDto> userDto = users.stream().
                map(o -> new UserDto(o.getName(),
                        o.getEmail(), o.getPhone()))
                .collect(Collectors.toList());*/
       // return bookRepo.findAll();
    }

    @GetMapping("/availableBook/{id}")
    public ResponseEntity<Integer> availableBook(@PathVariable Integer id)  {
        LOGGER.info("get single available Book");
        Optional<Book> bookFound  = bookRepo.findById(id);
        if(bookFound.isPresent()){
            Book book = bookFound.get();
            return ResponseEntity.ok(book.getTotalCopies() - book.getIssuedCopies());
        }
        LOGGER.info("not_available");
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/updateIssueBook/{id}")
    public ResponseEntity<Book>updateIssueBook(@PathVariable Integer id, Integer noOfCopies){
        LOGGER.info("Fetch single book from DB based on id");
        LOGGER.info("No of copies in bookms " + noOfCopies);
        Optional<Book> bookFound  = bookRepo.findById(id);
        if(bookFound.isPresent()){
            LOGGER.info("Book found");
            Book book = bookFound.get();
            book.setIssuedCopies(book.getIssuedCopies() + noOfCopies);
            LOGGER.info("ISBN in bookms " + book.getIsbn());
            bookRepo.save(book);
            return ResponseEntity.ok(book);
        }
        LOGGER.info("Book not found by id ", id);
        return ResponseEntity.notFound().build();
    }


    @GetMapping("/book/{id}")
    public ResponseEntity<Book> getSingleBook(@PathVariable Integer id){
        LOGGER.info("Fetch single Book from DB based on id");
        Optional<Book> bookFound  = bookRepo.findById(id);
        if(bookFound.isPresent()){
            LOGGER.info("Book found");
            return ResponseEntity.ok(bookFound.get());
        }
        LOGGER.info("Book not found by id ", id);
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/book")
    public ResponseEntity<Book>addBook(@RequestBody Book book) throws URISyntaxException {
        LOGGER.info("Saving Student");
        book.setId(null);
        Book bookStudent = bookRepo.save(book);
        return ResponseEntity.created(new URI(bookStudent.getId().toString())).body(bookStudent);
    }

    @DeleteMapping("/deleteBook/{id}")
    public ResponseEntity<String> deleteBook(@PathVariable Integer id)  {
        LOGGER.info("Delete Book");
        Optional<Book> bookFound  = bookRepo.findById(id);
        if(bookFound.isPresent()){
            bookRepo.deleteById(id);
            return ResponseEntity.ok("Book deleted by " + id);
        }
        LOGGER.info("Book not found by id ", id);
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/updateBook/{id}")
    public ResponseEntity<Book>updateBook(@PathVariable Integer id, @RequestBody Book bookDetails){
        LOGGER.info("Fetch single book from DB based on id");
        Optional<Book> bookFound  = bookRepo.findById(id);
        if(bookFound.isPresent()){
            LOGGER.info("Book found");
            Book book = bookFound.get();
            book.setAuthor(bookDetails.getAuthor());
            book.setTitle(bookDetails.getTitle());
            bookRepo.save(book);
            return ResponseEntity.ok(book);
        }
        LOGGER.info("Book not found by id ", id);
        return ResponseEntity.notFound().build();
    }


    @GetMapping("/hello")
    public String getHello() {
        System.out.println("****************");
        return "Hello World!";
    }

}
